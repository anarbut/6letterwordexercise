﻿# 6 letter words
## Quick start
### Either via Docker
Run the following commands from the repository root:
```
docker build -t word-app .
docker run -p 8088:80 word-app
```
Go to http://localhost:8088/swagger/index.html
> Use http because SSL is not supported yet

### or Visual Studio
Select IIS Express profile and hit F5. Swagger should be opened automatically in your browser.

## Features
- Use `GET /Word/FromString` endpoint to find possible solutons of the given words:
    1. Provide **input** string. It should consists of all strings separated with **separator**, f.e. `'foobar|fo|obar'`
    1. Provide **separator** string, f.e. `'|'`
    1. Provide **targetLength** - your desired target solution length. F.e. `6`
    1. Hit `Execute` and enjoy the results, f.e. `foobar=fo+obar`
- Use `POST /Word/FromFile` endpoint to upload your lovely **input.txt** from repository root. You'll get results in response, Swagger allows you to save the results into a file.
> be patient, it might take some time

## Ways to improve:
- Add user interface
- Fix TODOs in code (for example - algorithm is slow now, it is possible to improve it by caching intermediate results).
- Add integration tests, maybe extend unit tests
- Proper error handling
- Add logging
- Add SSL support so your uploads are protected for some time
- There is no limit to perfection...

## Original Task

There's a file in the root of the repository, input.txt, that contains words of varying lengths (1 to 6 characters).

Your objective is to show all combinations of those words that together form a word of 6 characters. That combination must also be present in input.txt  
E.g.:

```
foobar
fo
obar
```

should result in the ouput:

```
fo+obar=foobar
```

You can start by only supporting combinations of two words and improve the algorithm at the end of the exercise to support any combinations.

Treat this exercise as if you were writing production code; think unit tests, SOLID, clean code and avoid primitive obsession. Be mindful of changing requirements like a different maximum combination length, or a different source of the input data.

Don't spend too much time on this. When submitting the exercise, briefly write down where you would improve the code if you were given more time.

## License

MIT