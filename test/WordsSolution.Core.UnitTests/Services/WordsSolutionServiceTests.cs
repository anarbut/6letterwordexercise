﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using FluentAssertions;
using Moq;
using WordsSolution.Core.Models;
using WordsSolution.Core.Providers.WordsProvider;
using WordsSolution.Core.Services;
using WordsSolution.Core.WordSolutionStrategies;
using Xunit;

namespace WordsSolution.Core.UnitTests.Services {
  public class WordsSolutionServiceTests {
    /*
     
     public async Task<WordSolution[]> FindAllWordSolutions(IWordsProvider wordsProvider, int targetLength) {
      var input = await wordsProvider.GetWords();
      if (!input.Any()) {
        return Array.Empty<WordSolution>();
      }

      return _wordsSolutionStrategy.FindAllWordSolutions(input, targetLength);
    }

     */

    private readonly Mock<IWordsSolutionStrategy> _wordsSolutionStrategyMock;
    private readonly Mock<IWordsProvider> _wordsProvider;

    public WordsSolutionServiceTests() {
      _wordsSolutionStrategyMock = new Mock<IWordsSolutionStrategy>();
      _wordsProvider = new Mock<IWordsProvider>();
    }

    [Theory]
    [InlineData(-1)]
    [InlineData(0)]
    [InlineData(1)]
    public async Task InvalidTargetLenght_ThrowsArgumentException(int targetLength) {
      var service = new WordsSolutionService(_wordsSolutionStrategyMock.Object);

      Func<Task> act = async () => await service.FindAllWordSolutions(_wordsProvider.Object, targetLength);

      await act.Should().ThrowAsync<ArgumentOutOfRangeException>();
    }

    [Fact]
    public async Task FindAllWordSolutions_CallsProviderAndPassesArgsToStrategy() {
      var words = new[] { "ab", "a", "b" };
      var targetLength = 3;
      var expectedResult = new[] { new WordSolution("ab", new[] { "a", "b" }) };
      _wordsProvider.Setup(x => x.GetWords()).ReturnsAsync(words);
      _wordsSolutionStrategyMock.Setup(x => x.FindAllWordSolutions(It.Is<string[]>(w => w == words), It.Is<int>(l => l == targetLength)))
        .Returns(expectedResult);
      var service = new WordsSolutionService(_wordsSolutionStrategyMock.Object);

      var result = await service.FindAllWordSolutions(_wordsProvider.Object, targetLength);

      result.Should().NotBeNull();
      result.Should().BeEquivalentTo(expectedResult);
    }
  }
}
