﻿using System;
using System.Linq;
using FluentAssertions;
using WordsSolution.Core.Providers;
using WordsSolution.Core.Utils;
using WordsSolution.Core.WordSolutionStrategies;
using Xunit;

namespace WordsSolution.Core.UnitTests.WordSolutionStrategies {
  public class SubstringWordsSolutionStrategyTests {
    [Fact]
    public void EmptyInput_EmptyResult() {
      var strategy = GetSubstringWordsSolutionStrategy();

      var solutions = strategy.FindAllWordSolutions(Array.Empty<string>(), 4);

      solutions.Should().BeEmpty();
    }

    [Theory]
    [InlineData(new[] { "abcd", "a", "b", "c", "d" }, 4, "abcd=a+b+c+d")]
    [InlineData(new[] { "abcd", "a", "b", "cd" }, 4, "abcd=a+b+cd")]
    [InlineData(new[] { "abcd", "a", "bc", "d" }, 4, "abcd=a+bc+d")]
    [InlineData(new[] { "abcd", "ab", "c", "d" }, 4, "abcd=ab+c+d")]
    [InlineData(new[] { "abcd", "a", "bcd" }, 4, "abcd=a+bcd")]
    [InlineData(new[] { "abcd", "ab", "cd" }, 4, "abcd=ab+cd")]
    [InlineData(new[] { "abcd", "abc", "d" }, 4, "abcd=abc+d")]
    [InlineData(new[] { "abcd", "a", "ab", "abc", "b", "bc", "bcd", "c", "cd", "d" }, 4, "abcd=a+b+c+d", "abcd=a+b+cd", "abcd=a+bc+d", "abcd=ab+c+d", "abcd=a+bcd", "abcd=ab+cd", "abcd=abc+d")]
    [InlineData(new[] { "zzz", "z", "z", "z" }, 3, "zzz=z+z+z")]
    [InlineData(new[] { "zzz", "z", "z", "z", "z" }, 3, "zzz=z+z+z")]
    public void SolutionsAvailable_AllPossibleSolutionsFound(string[] input, int targetLength, params string[] expectedResults) {
      var strategy = GetSubstringWordsSolutionStrategy();

      var solutions = strategy.FindAllWordSolutions(input, targetLength);

      solutions.Should().NotBeNull();
      solutions.Should().HaveCount(expectedResults.Length);
      solutions.Select(x => x.ToString())
        .Should().BeEquivalentTo(expectedResults);
    }

    [Theory]
    [InlineData(new[] { "abc", "ab", "bc" }, 4)]
    [InlineData(new[] { "aaaa", "a", "a", "a" }, 4)]
    public void NoSolution_NotFound(string[] input, int targetLength) {
      var strategy = GetSubstringWordsSolutionStrategy();

      var solutions = strategy.FindAllWordSolutions(input, targetLength);

      solutions.Should().BeEmpty();
    }

    private IWordsSolutionStrategy GetSubstringWordsSolutionStrategy() {
      // TODO: configure dependency injection in tests and then mock deps if it needed
      var comparisonProvider = new OrdinalStringComparisonProvider();

      return new SubstringWordsSolutionStrategy(new WordCounter(comparisonProvider), comparisonProvider);
    }
  }
}
