﻿FROM mcr.microsoft.com/dotnet/aspnet:3.1 AS base
WORKDIR /app
EXPOSE 80
EXPOSE 443

FROM mcr.microsoft.com/dotnet/sdk:3.1 AS build
WORKDIR /src
COPY ["NuGet.Config", "."]
COPY ["src/WordsSolution.WebApp/WordsSolution.WebApp.csproj", "src/WordsSolution.WebApp/"]
COPY ["src/WordsSolution.Core/WordsSolution.Core.csproj", "src/WordsSolution.Core/"]
RUN dotnet restore "src/WordsSolution.WebApp/WordsSolution.WebApp.csproj"
COPY . .
WORKDIR "/src/src/WordsSolution.WebApp"
RUN dotnet build "WordsSolution.WebApp.csproj" -c Release -o /app/build

FROM build AS publish
RUN dotnet publish "WordsSolution.WebApp.csproj" -c Release -o /app/publish

FROM base AS final
WORKDIR /app
COPY --from=publish /app/publish .
ENTRYPOINT ["dotnet", "WordsSolution.WebApp.dll"]