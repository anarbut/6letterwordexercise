﻿using System;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using WordsSolution.Core.Models;
using WordsSolution.Core.Providers.WordsProvider;
using WordsSolution.Core.Services;

namespace WordsSolution.WebApp.Controllers {
  [Route("[controller]/[action]")]
  [ApiController]
  public class WordController : ControllerBase {
    private readonly IWordsProviderFactory _wordsProviderFactory;
    private readonly IWordsSolutionService _wordsSolutionService;

    public WordController(IWordsProviderFactory wordsProviderFactory, IWordsSolutionService wordsSolutionService) {
      (_wordsProviderFactory, _wordsSolutionService) = (wordsProviderFactory, wordsSolutionService);
    }

    [HttpGet]
    public async Task<string> FromString(string input = "abc|a|b|c", int targetLength = 3, string separator = "|") {
      var provider = _wordsProviderFactory.CreateStringProvider(input, separator);
      var solutions = await _wordsSolutionService.FindAllWordSolutions(provider, targetLength);

      return AggregateSolutions(solutions);
    }

    [HttpPost]
    public async Task<string> FromFile([FromForm] IFormFile file, CancellationToken cancellationToken, int targetLength = 6) {
      using var stream = file.OpenReadStream();
      var provider = _wordsProviderFactory.CreateStreamProvider(stream, cancellationToken);
      var solutions = await _wordsSolutionService.FindAllWordSolutions(provider, targetLength);

      return AggregateSolutions(solutions);
    }

    private string AggregateSolutions(WordSolution[] solutions) {
      var result = new StringBuilder();

      foreach (var solution in solutions) {
        result.AppendLine(solution.ToString());
      }

      return result.ToString();
    }
  }
}
