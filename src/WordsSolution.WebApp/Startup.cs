﻿using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using WordsSolution.WebApp.Swagger;

namespace WordsSolution.WebApp {
  public class Startup {
    public void ConfigureServices(IServiceCollection services) {
      Core.Startup.ConfigureServices(services);

      services.AddControllers();

      services.AddSwaggerGen(options => {
        options.OperationFilter<SwaggerFileOperationFilter>();
      });
    }

    public void Configure(IApplicationBuilder app, IWebHostEnvironment env) {
      app.UseSwagger();
      app.UseSwaggerUI(c => {
        c.SwaggerEndpoint("/swagger/v1/swagger.json", "My API V1");
      });

      if (env.IsDevelopment()) {
        app.UseDeveloperExceptionPage();
      }

      app.UseRouting();

      app.UseEndpoints(endpoints => {
        endpoints.MapControllers();
      });
    }
  }
}
