﻿using System;

namespace WordsSolution.Core.Providers {
  public interface IStringComparisonProvider {
    StringComparison GetComparison();
  }

  internal class OrdinalStringComparisonProvider: IStringComparisonProvider {
    public StringComparison GetComparison() {
      return StringComparison.Ordinal;
    }
  }
}
