﻿using System.Threading.Tasks;

namespace WordsSolution.Core.Providers.WordsProvider {
  public interface IWordsProvider {
    Task<string[]> GetWords();
  }
}
