﻿using System.Collections.Generic;
using System.IO;
using System.Threading;
using System.Threading.Tasks;

namespace WordsSolution.Core.Providers.WordsProvider {
  internal class StreamWordsProvider : IWordsProvider {
    private readonly Stream _stream;
    private readonly CancellationToken _cancellationToken;

    public StreamWordsProvider(Stream stream, CancellationToken cancellationToken) {
      _stream = stream;
      _cancellationToken = cancellationToken;
    }

    public async Task<string[]> GetWords() {
      using var streamReader = new StreamReader(_stream);

      var result = new List<string>();

      while(true) {
        _cancellationToken.ThrowIfCancellationRequested();

        var line = await streamReader.ReadLineAsync();

        if (line == null) {
          break;
        }

        if (!string.IsNullOrWhiteSpace(line)) {
          result.Add(line.Trim());
        }
      }

      return result.ToArray();
    }
  }
}
