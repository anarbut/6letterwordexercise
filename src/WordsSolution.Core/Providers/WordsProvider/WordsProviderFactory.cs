﻿using System.IO;
using System.Threading;

namespace WordsSolution.Core.Providers.WordsProvider {
  public interface IWordsProviderFactory {
    IWordsProvider CreateStreamProvider(Stream stream, CancellationToken cancellationToken);
    IWordsProvider CreateStringProvider(string input, string separator);
  }

  public class WordsProviderFactory : IWordsProviderFactory {
    public IWordsProvider CreateStreamProvider(Stream stream, CancellationToken cancellationToken) {
      return new StreamWordsProvider(stream, cancellationToken);
    }
    public IWordsProvider CreateStringProvider(string input, string separator) {
      return new StringWordsProvider(input, separator);
    }
  }
}
