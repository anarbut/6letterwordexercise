﻿using System;
using System.Threading.Tasks;

namespace WordsSolution.Core.Providers.WordsProvider {
  internal class StringWordsProvider: IWordsProvider {
    private readonly string _words;
    private readonly string _separator;

    public StringWordsProvider(string words, string separator) {
      (_words, _separator) = (words, separator);
    }

    public Task<string[]> GetWords() {
      return Task.FromResult(_words.Split(_separator, StringSplitOptions.RemoveEmptyEntries));
    }
  }
}
