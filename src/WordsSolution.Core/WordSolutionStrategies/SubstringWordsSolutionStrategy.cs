﻿using System;
using System.Collections.Generic;
using System.Linq;
using WordsSolution.Core.Models;
using WordsSolution.Core.Providers;
using WordsSolution.Core.Utils;
using static System.Linq.Enumerable;

namespace WordsSolution.Core.WordSolutionStrategies {
  internal class SubstringWordsSolutionStrategy : IWordsSolutionStrategy {
    private readonly IWordCounter _wordCounter;
    private readonly StringComparison _stringComparison;

    public SubstringWordsSolutionStrategy(IWordCounter wordCounter, IStringComparisonProvider stringComparisonProvider) {
      _wordCounter = wordCounter;
      _stringComparison = stringComparisonProvider.GetComparison();
    }

    public WordSolution[] FindAllWordSolutions(string[] input, int targetLength) {
      if (!input.Any()) {
        return Array.Empty<WordSolution>();
      }

      var availableWords = _wordCounter.CountWords(input, targetLength);

      // TODO: may be done in parallel
      return input
        .Where(item => item.Length == targetLength)
        .SelectMany(item => FindWordSolutions(item, availableWords, new[] { item }))
        .ToArray();
    }

    /// <summary>
    /// Recursively iterate over all combinations of substrings of targetString. Checks if each substring is available. Accounts remainders.
    /// </summary>
    /// <param name="targetString">String to find solution from available substrings</param>
    /// <param name="availableWords">Available words</param>
    /// <param name="usedStrings">Used words for current </param>
    /// <returns>Array of available <see cref="WordSolution"/></returns>
    private WordSolution[] FindWordSolutions(string targetString, Dictionary<string, int> availableWords, IEnumerable<string> usedStrings) {
      var result = new List<WordSolution>();

      // TODO: don't iterate over already calculated results, maybe use cache
      foreach (var prefixLength in Range(1, targetString.Length - 1)) {
        var prefix = targetString.Substring(0, prefixLength);
        if (!IsSubstringAvailable(prefix, availableWords, usedStrings)) {
          continue;
        }

        var suffix = targetString[prefixLength..];

        if (IsSubstringAvailable(suffix, availableWords, usedStrings.Append(prefix))) {
          result.Add(new WordSolution(targetString, new[] { prefix, suffix }));
        }

        var suffixSolutions = FindWordSolutions(suffix, availableWords, usedStrings.Append(prefix));
        foreach (var suffixSolution in suffixSolutions) {
          if (suffixSolution.Parts.Any()) {
            result.Add(new WordSolution(targetString, new[] { prefix }.Concat(suffixSolution.Parts).ToArray()));
          }
        }
      }

      return result.ToArray();
    }

    private bool IsSubstringAvailable(string substring, Dictionary<string, int> availableWords, IEnumerable<string> usedStrings) {
      var availableCount = availableWords.GetValueOrDefault(substring);
      if (availableCount == 0) {
        return false;
      }

      return availableCount > usedStrings.Count(used => string.Equals(used, substring, _stringComparison));
    }
  }
}
