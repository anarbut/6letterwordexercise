﻿using WordsSolution.Core.Models;

namespace WordsSolution.Core.WordSolutionStrategies {
  public interface IWordsSolutionStrategy {
    WordSolution[] FindAllWordSolutions(string[] input, int targetLength);
  }
}
