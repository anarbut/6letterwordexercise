﻿using System;
using System.Linq;
using System.Threading.Tasks;
using WordsSolution.Core.Models;
using WordsSolution.Core.Providers.WordsProvider;
using WordsSolution.Core.WordSolutionStrategies;

namespace WordsSolution.Core.Services {
  public interface IWordsSolutionService {
    Task<WordSolution[]> FindAllWordSolutions(IWordsProvider wordsProvider, int targetWordLength);
  }

  internal class WordsSolutionService: IWordsSolutionService {
    private readonly IWordsSolutionStrategy _wordsSolutionStrategy;

    public WordsSolutionService(IWordsSolutionStrategy wordsSolutionStrategy) {
      _wordsSolutionStrategy = wordsSolutionStrategy;
    }

    public async Task<WordSolution[]> FindAllWordSolutions(IWordsProvider wordsProvider, int targetLength) {
      if (targetLength <= 1) {
        throw new ArgumentOutOfRangeException(nameof(targetLength));
      }

      var input = await wordsProvider.GetWords();

      return _wordsSolutionStrategy.FindAllWordSolutions(input, targetLength);
    }
  }
}
