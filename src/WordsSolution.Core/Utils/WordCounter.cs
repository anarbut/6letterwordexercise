﻿using System;
using System.Collections.Generic;
using System.Linq;
using WordsSolution.Core.Providers;

namespace WordsSolution.Core.Utils {
  internal interface IWordCounter {
    Dictionary<string, int> CountWords(string[] input, int maxWordLength);
  }

  internal class WordCounter: IWordCounter {
    private readonly IStringComparisonProvider _stringComparisonProvider;

    public WordCounter(IStringComparisonProvider stringComparisonProvider) {
      _stringComparisonProvider = stringComparisonProvider;
    }

    public Dictionary<string, int> CountWords(string[] input, int maxWordLength) {
      var result = new Dictionary<string, int>(StringComparer.FromComparison(_stringComparisonProvider.GetComparison()));

      foreach (var item in input.Where(x => x.Length < maxWordLength)) {
        if (result.ContainsKey(item)) {
          result[item]++;
        }
        else {
          result.Add(item, 1);
        }
      }

      return result;
    }
  }
}
