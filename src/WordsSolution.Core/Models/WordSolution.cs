﻿namespace WordsSolution.Core.Models {
  public class WordSolution {
    public WordSolution(string solution, string[] parts) {
      (Solution, Parts) = (solution, parts);
    }

    public string Solution { get; }
    public string[] Parts { get; } // TODO: readonly collection

    public override string ToString() {
      return $"{Solution}={string.Join('+', Parts)}";
    }
  }
}
