﻿using System.Runtime.CompilerServices;
using Microsoft.Extensions.DependencyInjection;
using WordsSolution.Core.Providers;
using WordsSolution.Core.Providers.WordsProvider;
using WordsSolution.Core.Services;
using WordsSolution.Core.Utils;
using WordsSolution.Core.WordSolutionStrategies;

[assembly: InternalsVisibleTo("WordsSolution.Core.UnitTests")]

namespace WordsSolution.Core {
  public class Startup {
    public static void ConfigureServices(IServiceCollection services) {
      services.AddSingleton<IWordCounter, WordCounter>();
      services.AddSingleton<IStringComparisonProvider, OrdinalStringComparisonProvider>();

      services.AddTransient<IWordsSolutionStrategy, SubstringWordsSolutionStrategy>();
      services.AddTransient<IWordsProviderFactory, WordsProviderFactory>();
      services.AddTransient<IWordsSolutionService, WordsSolutionService>();
    }
  }
}
